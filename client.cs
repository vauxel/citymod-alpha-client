// ============================================================
// Author			-		Vaux
// Description		-		Main Client Script
// ============================================================
// Sections
// 	 1: Profiles/Loading
//   2: ClientCmds
//   3: Functions
//   4: Package
// ============================================================

// ============================================================
// Section 1 - Profiles/Loading
// ============================================================

$CityModClient::isActive = 0;
$CityModClient::Password = "c64WDkvQ";
$CityModClient::Version = "1.2";

// ============================================================

if(!isObject(CMWindowProfile))
{
	new GuiControlProfile(CMWindowProfile : blockWindowProfile)
	{
		opaque = "0";
		fillColor = "238 238 238 255";
		fillColorHL = "238 238 238 255";
		fillColorNA = "238 238 238 255";
		fontType = "Impact";
		fontSize = "18";
		fontColor = "238 238 238 255";
		fontColorHL = "238 238 238 255";
		fontColorNA = "238 238 238 255";
		textOffset = "4 4";
		canMaximize = "0";
		canMinimize = "0";
		bitmap = "Add-Ons/Client_CityMod/assets/ui/citymodWindow";
	};
	new GuiControlProfile(CMTextEditProfile : GuiTextEditProfile)
	{
		fontColor = "30 30 30 255";
		fontSize = 12;
		fontType = "Verdana Bold";
		justify = "Left";
		fontColors[1] = "150 150 150";
		fontColors[2] = "0 255 0";  
		fontColors[3] = "0 0 255"; 
		fontColors[4] = "255 255 0";
		fillColor = "0 0 0 0";
 	    fillColorHL = "0 0 0 0";
	    fillColorNA = "0 0 0 0";
	    border = "0";
	};
	new GuiControlProfile(CMBitmapButtonProfile : BlockButtonProfile)
	{
		fontColor = "255 255 255 255";
	};
	new GuiControlProfile(CMScrollProfile)
	{
		tab = "0";
	    canKeyFocus = "0";
	    mouseOverSelected = "0";
	    modal = "1";
	    opaque = "0";
	    fillColor = "255 255 255 0";
	    fillColorHL = "200 200 200 255";
	    fillColorNA = "200 200 200 255";
	    fontType = "Verdana Bold";
	    fontSize = "13";
	    fontColors[0] = "0 0 0 255";
	    fontColors[1] = "32 100 100 255";
	    fontColors[2] = "0 0 0 255";
	    fontColors[3] = "200 200 200 255";
	    fontColors[4] = "0 0 204 255";
	    fontColors[5] = "85 26 139 255";
	    fontColors[6] = "0 0 0 0";
 	    fontColors[7] = "0 0 0 0";
	    fontColors[8] = "0 0 0 0";
	    fontColors[9] = "0 0 0 0";
	    fontColor = "76 76 76 255";
	    fontColorHL = "32 100 100 255";
	    fontColorNA = "0 0 0 255";
	    fontColorSEL = "200 200 200 255";
	    fontColorLink = "0 0 204 255";
	    fontColorLinkHL = "85 26 139 255";
	    doFontOutline = "0";
	    fontOutlineColor = "255 255 255 255";
	    justify = "center";
	    textOffset = "0 0";
	    autoSizeWidth = "0";
	    autoSizeHeight = "0";
	    returnTab = "0";
	    numbersOnly = "0";
	    bitmap = "./assets/ui/citymodScroll.png";
		hasBitmapArray = "1";
	};
	new GuiControlProfile(CMButtonProfile) 
	{
	    tab = "0";
	    canKeyFocus = "0";
	    mouseOverSelected = "0";
	    modal = "1";
	    opaque = "1";
	    fillColor = "149 152 166 255";
 	    fillColorHL = "171 171 171 255";
	    fillColorNA = "221 202 173 255";
	    border = "1";
	    borderThickness = "1";
	    borderColor = "0 0 0 255";
	    borderColorHL = "128 128 128 255";
	    borderColorNA = "64 64 64 255";
	    fontType = "Arial Black";
	    fontSize = "18";
	    fontColors[0] = "0 0 0 255";
	    fontColors[1] = "255 255 255 255";
	    fontColors[2] = "0 0 0 255";
	    fontColors[3] = "200 200 200 255";
	    fontColors[4] = "0 0 204 255";
	    fontColors[5] = "85 26 139 255";
	    fontColors[6] = "0 0 0 0";
	    fontColors[7] = "0 0 0 0";
	    fontColors[8] = "0 0 0 0";
	    fontColors[9] = "0 0 0 0";
	    fontColor = "46 62 72 255";
	    fontColorHL = "46 62 72 255";
	    fontColorNA = "46 62 72 255";
	    fontColorSEL = "46 62 72 255";
	    fontColorLink = "0 0 204 255";
	    fontColorLinkHL = "85 26 139 255";
	    doFontOutline = "0";
	    fontOutlineColor = "255 255 255 255";
	    justify = "center";
	    textOffset = "6 6";
	    autoSizeWidth = "0";
	    autoSizeHeight = "0";
	    returnTab = "0";
	    numbersOnly = "0";
	    cursorColor = "0 0 0 255";
	    bitmap = "./assets/ui/Button1";
		text = "CityMod Button";
		hasBitmapArray = "1";
	};
	new GuiControlProfile(CMProgressBar : RTB_CCGroupEditProfile)
	{
	   fillColor = "0 200 0 100";
	};
	new GuiControlProfile(CMSmallButtonProfile : CMButtonProfile){ fontSize = "16"; fontColor = "255 255 255 255"; bitmap = "./assets/ui/Button3"; };
}

// ============================================================

exec("./support/Support_UpdaterDownload.cs");

exec("./guis/CM_MessageBox.gui");
exec("./scripts/hud.cs");
exec("./scripts/notifications.cs");
exec("./scripts/realestate.cs");
exec("./scripts/government.cs");
exec("./scripts/bank.cs");
exec("./scripts/school.cs");
exec("./scripts/machinery.cs");
exec("./scripts/checkout.cs");
exec("./scripts/police.cs");
exec("./scripts/group.cs");

// ============================================================
// Section 2 - ClientCmds
// ============================================================

function clientcmdCM_openGUI(%type)
{
	if(%type $= "")
		return;
		
	switch$(strLwr(%type))
	{
		case "realestate": canvas.pushDialog(CM_RealEstate);
		case "bank": canvas.pushDialog(CM_Bank);
		case "machinery": canvas.pushDialog(CM_Machinery);
		case "cashregister": canvas.pushDialog(CM_Checkout);
		case "police": canvas.pushDialog(CM_Police);
		case "voting": canvas.pushDialog(CM_Voting);
		case "school": canvas.pushDialog(CM_School);
		case "groups": canvas.pushDialog(CM_Group);
		default: warn("CMCLient:ERROR - " @ %type SPC "is not a valid GUI file!");
	}
}

function clientcmdCM_closeGUI(%type)
{
	if(%type $= "")
		return;
		
	switch$(strLwr(%type))
	{
		case "realestate": canvas.popDialog(CM_RealEstate);
		case "bank": canvas.popDialog(CM_Bank);
		case "machinery": canvas.popDialog(CM_Machinery);
		case "cashregister": canvas.popDialog(CM_Checkout);
		case "police": canvas.popDialog(CM_Police);
		case "voting": canvas.popDialog(CM_Voting);
		case "school": canvas.popDialog(CM_School);
		case "groups": canvas.popDialog(CM_Group);
		default: warn("CMCLient:ERROR - " @ %type SPC "is not a valid GUI file!");
	}
}

function clientcmdCM_pushNotifyBox(%header, %text)
{
	NotificationBox.setText(%header);
	NotificationText.setText("<just:center><font:Berlin Sans FB Demi:18><color:4C4C4C>" @ %text);

	NotificationBox.resize(
		getWord(NotificationBox.getPosition(), 0),
		getWord(NotificationBox.getPosition(), 1),
		getWord(NotificationBox.getExtent(), 0),
		161 + strLen(%text)
	);

	NotificationButton.resize(
		getWord(NotificationButton.getPosition(), 0),
		(getWord(NotificationBox.getExtent(), 1) - 36),
		getWord(NotificationButton.getExtent(), 0),
		getWord(NotificationButton.getExtent(), 1)
	);

	canvas.pushDialog(CM_MessageBox);
	CM_MessageBox.centerY();
}

function clientcmdCM_Notify(%title, %text, %icon)
{
	//All credit goes to RTB for the Notifications
	CM_NotificationManager.push(%title, %text, %icon);
}

// ============================================================
// Section 3 - Functions
// ============================================================

function CM_Cleanup()
{
	newChatHud.remove("CM_HUD");
	deleteVariables("$CityModClient::Temp*");
	$CityModClient::IsActive = 0;
}

function CM_ResizeHUD()
{
	CM_HUD.resize(
		0, 
		(getWord($Pref::Video::Resolution, 1) / 2) - mFloor(getWord(CM_HUD.getExtent(), 1) / 2), 
		getWord($Pref::Video::Resolution, 0), 
		getWord(CM_HUD.getExtent(), 1)
	);

	InventoryBox.resize(
		(getWord($Pref::Video::Resolution, 0) - firstWord(InventoryBox.getExtent())), 
		getWord(InventoryBox.getPosition(), 1), 
		getWord(InventoryBox.getExtent(), 0), 
		getWord(InventoryBox.getExtent(), 1)
	);

	InventoryBox.isOpen = true;
}

function valueColor(%value)
{
	if(%value < 0)
		return "<color:FF2222>";
	else if(%value > 0)
		return "<color:66CC66>";
	else
		return "<color:333333>";
}

function properText(%text)
{
	if(strLen(%text) == 1)
		return %text;

	return strUpr(getSubStr(%text, 0, 1)) @ strLwr(getSubStr(%text, 1, strLen(%text)));
}

function condenseValue(%num)
{
	if(mAbs(%num) < 1000) return %num;
	
	%neg = %num < 0 ? -1 : 1;
	%num = mAbs(%num);
	%suffix = "";
	
	if(%num >= 1000000000)
	{
		%num /= 1000000000;
		%suffix = "b";
	}
	else if(%num >= 1000000)
	{
		%num /= 1000000;
		%suffix = "m";
	}
	else if(%num >= 1000)
	{
		%num /= 1000;
		%suffix = "k";
	}

	%num = mFloatLength(%num, 1);
	
	if(mFloor(%num - 0.1) < mFloor(%num))
		%num = mFloor(%num);
	
	%num *= %neg;
	
	return %num @ %suffix;
}

function isInteger(%string)
{
	%search = "0 1 2 3 4 5 6 7 8 9";
	for(%i = 0; %i < getWordCount(%search); %i++)
	{
		%string = strReplace(%string, getWord(%search, %i), "");
	}
	if(%string $= "")
		return true;
	return false;
}

function GuiControl::setCenteredY(%this)
{
	%parent = %this.getGroup();
	if(!isObject(%parent))
		return;

	%parExtY = getWord(%parent.getExtent(), 1);
	%extY = getWord(%this.getExtent(), 1);
	%posY = (%parExtY / 2) - (%extY / 2);

	%this.position = getWord(%this.getPosition(), 0) SPC %posY;
}

// ============================================================
// Section 4 - Package
// ============================================================

package CM_ClientPackage
{
	function disconnectedCleanup()
	{
		%return = parent::disconnectedCleanup();
		CM_Cleanup();
		return %return;
	}

	function resetCanvas()
	{
		%return = parent::resetCanvas();

		if($CityModClient::isActive)
			CM_ResizeHUD();

		return %return;
	}
};
if(isPackage(CM_ClientPackage))
	deactivatepackage(CM_ClientPackage);
activatepackage(CM_ClientPackage);