// ============================================================
// Author				-		Vaux
// Description			-		Code for the Bank GUI
// ============================================================
exec("Add-Ons/Client_CityMod/guis/CM_RealEstate.gui");

function clientcmdCM_addLotList(%brick, %id, %owner, %type, %cost)
{
	$CityModClient::Temp::LotData[%id] = new scriptObject()
	{
		Class = "CMLotData";
		
		BrickID = %brick;
		LotID = %id;
		Owner = %owner;
		Type = %type;
		Cost = %cost;
	};
	
	LotsList.addRow(1, getSubStr(%type, 0, 3) @ "." SPC "Lot #" @ %id SPC "FSBO" SPC %owner SPC "for $" @ %cost);
}

function clientcmdCM_callREUpdate()
{
	LotsList.clear();
	commandtoserver('CM_requestRE');
}

function CM_RealEstate::onWake(%this)
{
	LotsList.clear();
	commandtoserver('CM_requestRE');
}

function CM_RealEstate::buyLot(%this)
{
	%id = getWord(LotsList.getValue(), 2);
	%id = strReplace(%id, "#", "");
	%data = $CityModClient::Temp::LotData[%id];
	
	if(LotsList.getSelectedID() $= "-1")
		return clientcmdCM_pushNotifyBox("Real Estate", "You didn't select a Lot to buy!");
	
	if(!isObject(%data))
		return clientcmdCM_pushNotifyBox("Real Estate", "Somehow you screwed up, you didn't select a real lot.");
	
	commandtoserver('CM_buyLot', %data.BrickID, %data.LotID);
}

function CM_RealEstate::sellLot(%this)
{
	%id = LotIDInput.getValue();
	%price = LotPriceInput.getValue();
	
	if(strStr(%id, "#") !$= "-1")
		%id = strReplace(%id, "#", "");

	echo("ID:" SPC %id);
	if((%id $= "") || (%id $= LotIDInput.text) || (!isInteger(%id)))
		return clientcmdCM_pushNotifyBox("Real Estate", "You didn't enter a valid Lot ID!");
	
	if((%price $= "") || (%price $= LotPriceInput.text) || (!isInteger(%price)))
		return clientcmdCM_pushNotifyBox("Real Estate", "You didn't enter a valid Price!");
	
	commandtoserver('CM_sellLot', %id, %price);
}

function CM_RealEstate::refresh(%this)
{
	if($CityModClient::Temp::RefreshTime $= "")
		$CityModClient::Temp::RefreshTime = $Sim::Time - 5;
		
	if($CityModClient::Temp::RefreshTime + 5 > $Sim::Time)
		return;
	
	LotsList.clear();
	commandtoserver('CM_requestRE');
	$CityModClient::Temp::RefreshTime = $Sim::Time;
}