// ============================================================
// Author				-		Vaux
// Description			-		Code for the Mayor/Voting GUI
// ============================================================

exec("Add-Ons/Client_CityMod/guis/CM_Voting.gui");

function clientcmdCM_setPVotingData(%daysleft, %c1blid, %c1name, %c1money, %c1votes, %c2blid, %c2name, %c2money, %c2votes)
{
	$CityModClient::Temp::PVotingData[1] = new scriptObject()
	{
		Class = "CMPVotingData";
		
		BL_ID = %c1blid;
		Name = %c1name;
		Bid = %c1money;
		Votes = %c1votes;
	};

	$CityModClient::Temp::PVotingData[2] = new scriptObject()
	{
		Class = "CMPVotingData";
		
		BL_ID = %c2blid;
		Name = %c2name;
		Bid = %c2money;
		Votes = %c2votes;
	};

	VotingSecondaryPanel.setVisible(0);
	VotingPrimaryPanel1.setVisible(1);
	VotingPrimaryPanel2.setVisible(1);

	VotingStatusText.setText("<just:center><font:Impact:26><color:3E3E3E>" @ %daysleft SPC "Day" @ (%daysleft > 1 ? "s" : "") SPC "Left to Vote!");

	PrimaryCanidate1Name.setText("<just:center><font:Impact:26>" @ %c1name);
	PrimaryCanidate2Name.setText("<just:center><font:Impact:26>" @ %c2name);

	PrimaryCanidate1Money.setText("<just:center><font:Impact:16>$" @ condenseValue(%c1money));
	PrimaryCanidate2Money.setText("<just:center><font:Impact:16>$" @ condenseValue(%c2money));

	PrimaryCanidate1VoteCount.setText("<color:ffffff><just:center><font:Impact:18>" @ %c1votes SPC "Votes");
	PrimaryCanidate2VoteCount.setText("<color:ffffff><just:center><font:Impact:18>" @ %c2votes SPC "Votes");

	PrimaryCanidate1Scale.extent = firstWord(PrimaryCanidate1Scale.extent) SPC (getWord(PrimaryCanidate1Scale.extent, 1) + (%c1votes *= 10));
	PrimaryCanidate2Scale.extent = firstWord(PrimaryCanidate2Scale.extent) SPC (getWord(PrimaryCanidate2Scale.extent, 1) + (%c2votes *= 10));
}

function clientcmdCM_setSVotingData(%daysleft, %numcanidates, %leader)
{
	VotingPrimaryPanel1.setVisible(0);
	VotingPrimaryPanel2.setVisible(0);
	VotingSecondaryPanel.setVisible(1);

	VotingStatusText.setText("<just:center><font:Impact:26><color:3E3E3E>" @ %daysleft SPC "Day" @ (%daysleft > 1 ? "s" : "") SPC "Left to Vote!");
	SecondaryCanidatesAmt.setText("<just:center><font:Impact:26>" @ %numcanidates SPC "Canidate" @ (%daysleft > 1 ? "s" : ""));
	SecondaryCanidatesLeader.setText("<just:center><font:Impact:16>" @ %leader SPC "is in the lead!");
}

function clientcmdCM_setSVoterData(%blid, %name, %votes)
{
	if(!SecondaryVoter1.isVisible())
	{
		$CityModClient::Temp::SVotingData[1] = new scriptObject()
		{
			Class = "CMSVotingData";
			
			BL_ID = %blid;
			Name = %name;
			Votes = %votes;
		};
		SecondaryVoterText1.setText("<just:center><font:Impact:22><color:3E3E3E>" @ %name @ "  --  " @ %votes SPC "Vote" @ (%votes > 1 ? "s" : ""));
		SecondaryVoter1.setVisible(1);
	}
	else if(!SecondaryVoter2.isVisible())
	{
		$CityModClient::Temp::SVotingData[2] = new scriptObject()
		{
			Class = "CMSVotingData";
			
			BL_ID = %blid;
			Name = %name;
			Votes = %votes;
		};
		SecondaryVoterText2.setText("<just:center><font:Impact:22><color:3E3E3E>" @ %name @ "  --  " @ %votes SPC "Vote" @ (%votes > 1 ? "s" : ""));
		SecondaryVoter2.setVisible(1);
	}
	else if(!SecondaryVoter3.isVisible())
	{
		$CityModClient::Temp::SVotingData[3] = new scriptObject()
		{
			Class = "CMSVotingData";
			
			BL_ID = %blid;
			Name = %name;
			Votes = %votes;
		};
		SecondaryVoterText3.setText("<just:center><font:Impact:22><color:3E3E3E>" @ %name @ "  --  " @ %votes SPC "Vote" @ (%votes > 1 ? "s" : ""));
		SecondaryVoter3.setVisible(1);
	}
	else if(!SecondaryVoter4.isVisible())
	{
		$CityModClient::Temp::SVotingData[4] = new scriptObject()
		{
			Class = "CMSVotingData";
			
			BL_ID = %blid;
			Name = %name;
			Votes = %votes;
		};
		SecondaryVoterText4.setText("<just:center><font:Impact:22><color:3E3E3E>" @ %name @ "  --  " @ %votes SPC "Vote" @ (%votes > 1 ? "s" : ""));
		SecondaryVoter4.setVisible(1);
	}
	else if(!SecondaryVoter5.isVisible())
	{
		$CityModClient::Temp::SVotingData[5] = new scriptObject()
		{
			Class = "CMSVotingData";
			
			BL_ID = %blid;
			Name = %name;
			Votes = %votes;
		};
		SecondaryVoterText5.setText("<just:center><font:Impact:22><color:3E3E3E>" @ %name @ "  --  " @ %votes SPC "Vote" @ (%votes > 1 ? "s" : ""));
		SecondaryVoter5.setVisible(1);
	}
	else if(!SecondaryVoter6.isVisible())
	{
		$CityModClient::Temp::SVotingData[6] = new scriptObject()
		{
			Class = "CMSVotingData";
			
			BL_ID = %blid;
			Name = %name;
			Votes = %votes;
		};
		SecondaryVoterText6.setText("<just:center><font:Impact:22><color:3E3E3E>" @ %name @ "  --  " @ %votes SPC "Vote" @ (%votes > 1 ? "s" : ""));
		SecondaryVoter6.setVisible(1);
	}
	else if(!SecondaryVoter7.isVisible())
	{
		$CityModClient::Temp::SVotingData[7] = new scriptObject()
		{
			Class = "CMSVotingData";
			
			BL_ID = %blid;
			Name = %name;
			Votes = %votes;
		};
		SecondaryVoterText7.setText("<just:center><font:Impact:22><color:3E3E3E>" @ %name @ "  --  " @ %votes SPC "Vote" @ (%votes > 1 ? "s" : ""));
		SecondaryVoter7.setVisible(1);
	}
}

function CM_Voting::onWake(%this)
{
	SecondaryVoter1.setVisible(0);
	SecondaryVoter2.setVisible(0);
	SecondaryVoter3.setVisible(0);
	SecondaryVoter4.setVisible(0);
	SecondaryVoter5.setVisible(0);
	SecondaryVoter6.setVisible(0);
	SecondaryVoter7.setVisible(0);

	commandtoserver('CM_requestVotingData');
}

function CM_Voting::primaryVote(%num)
{
	if(%num $= "" || !isInteger(%num) || (%num != 1 && %num != 2))
		return;

	commandtoserver('CM_castPrimaryVote', $CityModClient::Temp::PVotingData[%num].BL_ID);
}

function CM_Voting::secondaryVote(%num)
{
	if(%num $= "" || !isInteger(%num) || %num > 7 || %num < 1)
		return;

	commandtoserver('CM_castSecondaryVote', $CityModClient::Temp::SVotingData[%num].BL_ID);
}