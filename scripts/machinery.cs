// ============================================================
// Author				-		Vaux
// Description			-		Code for the Machinery GUI
// ============================================================

exec("Add-Ons/Client_CityMod/guis/CM_Machinery.gui");

function clientcmdCM_machineryUpdate(%value)
{
	MachineProgressBar.setValue(%value);
	MachineStatusText.setText("<just:center><font:Impact:25><color:5E5E5E>Machinery Working...");
}

function clientcmdCM_machineryDone(%value)
{
	MachineStatusText.setText("<just:center><font:Impact:25><color:5E5E5E>Machinery Finished");
}

function CM_Machinery::onSleep(%this)
{
	commandtoserver('CM_removeMachineLoop');
}

function CM_Machinery::onWake(%this)
{
	MachineProgressBar.setValue(1);
}