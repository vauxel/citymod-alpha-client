// ============================================================
// Author				-		Vaux
// Description			-		Code for the School GUI
// ============================================================

exec("Add-Ons/Client_CityMod/guis/CM_School.gui");

function clientcmdCM_clearSkillsList()
{
	SkillScroll1.deleteAll();
	SkillScroll2.deleteAll();
	SkillScroll3.deleteAll();
	SkillScroll4.deleteAll();
	SkillScroll5.deleteAll();
	SkillScroll6.deleteAll();

	SkillScroll1.resize(0, 1, 126, 180);
	SkillScroll2.resize(0, 1, 126, 180);
	SkillScroll3.resize(0, 1, 126, 180);
	SkillScroll4.resize(0, 1, 126, 180);
	SkillScroll5.resize(0, 1, 126, 180);
	SkillScroll6.resize(0, 1, 126, 180);
}

function clientcmdCM_addSkill(%skillset, %skillname, %reqs)
{
	%skillsetnames["Business"] = "1";
	%skillsetnames["Merchant"] = "2";
	%skillsetnames["Law"] = "3";
	%skillsetnames["Medical"] = "4";
	%skillsetnames["Government"] = "5";
	%skillsetnames["Labor"] = "6";

	%scroll = "SkillScroll" @ %skillsetnames[%skillset];

	if(!isObject(%scroll))
		return;

	if(%scroll.getCount() <= 0)
		%pos = "1 0";
	else
		%pos = 1 SPC getWord(%scroll.getObject(%scroll.getCount() - 1).position, 1) + getWord(%scroll.getObject(%scroll.getCount() - 1).extent, 1) + 1;

	%skillbox = new GuiBitmapBorderCtrl()
	{
		profile = "RTB_ContentBorderProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = %pos;
		extent = "107 59";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
	};

	%header = new GuiMLTextCtrl()
	{
		profile = "GuiMLTextProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "0 2";
		extent = "107 28";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		lineSpacing = "2";
		allowColorChars = "0";
		maxChars = "-1";
		text = "<just:center><font:Impact:14>" @ %skillname @ "<br>REQ:" SPC %reqs;
		maxBitmapHeight = "-1";
		selectable = "1";
		autoResize = "1";
	};

	%divider = new GuiBitmapCtrl()
	{
		profile = "GuiDefaultProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "1 31";
		extent = "105 6";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		bitmap = "~/Client_CityMod/assets/ui/divider";
		wrap = "0";
		lockAspectRatio = "0";
		alignLeft = "0";
		alignTop = "0";
		overflowImage = "0";
		keepCached = "0";
		mColor = "255 255 255 255";
		mMultiply = "0";
	};

	%skillbox.add(%header);
	%skillbox.add(%divider);

	%button = new GuiBitmapButtonCtrl()
	{
		profile = "CMSmallButtonProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "14 38";
		extent = "80 17";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		command = "";
		text = "_______";
		groupNum = "-1";
		buttonType = "PushButton";
		bitmap = "~/Client_CityMod/assets/ui/Button3";
		lockAspectRatio = "0";
		alignLeft = "0";
		alignTop = "0";
		overflowImage = "0";
		mKeepCached = "0";
		mColor = "255 255 255 255";
	};

	%buttontext = new GuiMLTextCtrl()
	{
		profile = "GuiMLTextProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "0 0";
		extent = "80 14";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		command = "CM_School.clickSkill(" @ %skillset @ "," SPC %scroll.getCount() @ ");";
		lineSpacing = "2";
		allowColorChars = "0";
		maxChars = "-1";
		text = "<just:center><font:Impact:14><color:ffffff>More Info";
		maxBitmapHeight = "-1";
		selectable = "1";
		autoResize = "1";
	};

	//Add the necessary elements
	%button.add(%buttontext);
	%skillbox.add(%button);

	if(%scroll.getCount() >= 3)
	{
		//Re-Adjust the Swatch
		%guiExtY = getWord(%scroll.extent, 1);
		%swatchExtY = getWord(%skillbox.extent, 1);
		%swatchPosY = getWord(%skillbox.position, 1);

		%extX = getWord(%scroll.extent, 0);
		%extY = %guiExtY + %swatchExtY + 1;
		%scroll.extent = %extX SPC %extY;
	}

	//Refresh the Swatch
	%scroll.add(%skillbox);
	%scroll.setVisible(1);
}

function CM_School::onWake(%this)
{
	clientcmdCM_clearSkillsList();

	commandtoserver('CM_requestSkills');
}

function CM_School::clickSkill(%this, %skillset, %id)
{
	if(%skillset $= "" || %id $= "" || !isInteger(%id))
		commandtoserver('CM_chooseSkill', %skillset, %id);
}

function CM_School::openInfoTab(%this)
{

}

function CM_School::openSkillsTab(%this)
{

}

function CM_School::openLicensesTab(%this)
{

}