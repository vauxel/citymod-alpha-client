// ============================================================
// Author				-		Vaux
// Description			-		Code for the HUD
// ============================================================

exec("Add-Ons/Client_CityMod/guis/CM_HUD.gui");

function clientcmdCM_openHUD()
{
	newChatHud.add("CM_HUD");
	$CityModClient::isActive = 1;

	CM_ResizeHUD();
	
	commandtoserver('CM_requestInventory');
	CM_HUD.toggleInventory();

	InventoryBox.isOpen = true;
	InventoryBox.isOpening = false;
}

function clientcmdCM_closeHUD()
{
	newChatHud.remove("CM_HUD");
	$CityModClient::isActive = 0;
}

function clientcmdCM_enterLot(%name, %id)
{
	%textmlchars = "<just:center><shadow:2:2><shadowcolor:00000066><color:EEEEEE>";
	
	LotBox.setVisible(1);
	LotName.setText(%textmlchars @ "<font:Impact:18>" @ %name);
	LotID.setText(%textmlchars @ "<font:Impact:14>" @  %id);
}

function clientcmdCM_leaveLot()
{
	LotBox.setVisible(0);
}

function clientcmdCM_updateInventory(%one, %two, %three, %four, %five, %six)
{
	%filepath = "Add-Ons/Client_CityMod/assets/images/inventory/";

	if(firstWord(%one) !$= "NONE")
	{
		InventoryAmt1.setText(getWord(%one, 1) NL properText(firstWord(%one)));
		%bitmap = isFile(%filepath @ firstWord(%one)) ? (%filepath @ firstWord(%one)) : (%filepath @ "Unknown.png");
	}
	else
	{
		InventoryAmt1.setText("Empty");
		%bitmap = "";
	}

	InventorySlot1.setBitmap(%bitmap);

	if(firstWord(%two) !$= "NONE")
	{
		InventoryAmt2.setText(getWord(%two, 1) NL properText(firstWord(%two)));
		%bitmap = isFile(%filepath @ firstWord(%two)) ? (%filepath @ firstWord(%two)) : (%filepath @ "Unknown.png");
	}
	else
	{
		InventoryAmt2.setText("Empty");
		%bitmap = "";
	}

	InventorySlot2.setBitmap(%bitmap);

	if(firstWord(%three) !$= "NONE")
	{
		InventoryAmt3.setText(getWord(%three, 1) NL properText(firstWord(%three)));
		%bitmap = isFile(%filepath @ firstWord(%three)) ? (%filepath @ firstWord(%three)) : (%filepath @ "Unknown.png");
	}
	else
	{
		InventoryAmt3.setText("Empty");
		%bitmap = "";
	}

	InventorySlot3.setBitmap(%bitmap);

	if(firstWord(%four) !$= "NONE")
	{
		InventoryAmt4.setText(getWord(%four, 1) NL properText(firstWord(%four)));
		%bitmap = isFile(%filepath @ firstWord(%four)) ? (%filepath @ firstWord(%four)) : (%filepath @ "Unknown.png");
	}
	else
	{
		InventoryAmt4.setText("Empty");
		%bitmap = "";
	}

	InventorySlot4.setBitmap(%bitmap);

	if(firstWord(%five) !$= "NONE")
	{
		InventoryAmt5.setText(getWord(%five, 1) NL properText(firstWord(%five)));
		%bitmap = isFile(%filepath @ firstWord(%five)) ? (%filepath @ firstWord(%five)) : (%filepath @ "Unknown.png");
	}
	else
	{
		InventoryAmt5.setText("Empty");
		%bitmap = "";
	}

	InventorySlot5.setBitmap(%bitmap);

	if(firstWord(%six) !$= "NONE")
	{
		InventoryAmt6.setText(getWord(%six, 1) NL properText(firstWord(%six)));
		%bitmap = isFile(%filepath @ firstWord(%six)) ? (%filepath @ firstWord(%six)) : (%filepath @ "Unknown.png");
	}
	else
	{
		InventoryAmt6.setText("Empty");
		%bitmap = "";
	}

	InventorySlot6.setBitmap(%bitmap);
}

function clientcmdCM_updateHUD(%money, %reputation, %demerits, %taxes, %income, %name, %skill, %health, %hunger)
{
	%textmlchars = "<just:center><shadow:2:2><shadowcolor:00000066><color:EEEEEE><font:Impact:16>";
	
	//Wallet
	WalletText.setText(%textmlchars @ "Wallet" @ " <color:999999> - " SPC valueColor(%money) @ "$" @ %money);
	//Reputation
	ReputationText.setText(%textmlchars @ "Reputation" @ " <color:999999> - " SPC valueColor(%reputation) @ "+" @ %reputation);
	//Demerits
	DemeritsText.setText(%textmlchars @ "Demerits" @ " <color:999999> - " SPC valueColor(%demerits) @ %demerits);
	//Taxes
	TaxesText.setText(%textmlchars @ "Taxes" @ " <color:999999> - " SPC valueColor(%taxes) @ "$" @ %taxes);
	//LastTickIncome
	IncomeText.setText(%textmlchars @ "Income" @ " <color:999999> - " SPC valueColor(%income) @ "$" @ %income);
	//Name
	NameText.setText(%textmlchars @ "Name" @ " <color:999999> - " SPC "<color:FFFF11>" @ %name);
	//Skill
	SkillText.setText(%textmlchars @ "Skill" @ " <color:999999> - " SPC "<color:FFD000>" @ %skill);
	//Health
		if(%health <= 10)
			%healthcolor = "FF2222";
		else if(%health <= 25 && %health > 10)
			%healthcolor = "FFD000";
		else if(%health <= 50 && %health > 25)
			%healthcolor = "FFCC00";
		else if(%health <= 75 && %health > 50)
			%healthcolor = "CCFF00";
		else if(%health <= 100 && %health > 75)
			%healthcolor = "55FF00";
		else if(%health $= "CRITICAL")
			%healthcolor = "FF0000";
		else
			%healthcolor = "FFFFFF";
	HealthText.setText(%textmlchars @ "Health" @ " <color:999999> - " SPC "<color:" @ %healthcolor @ ">" @ %health @ (%health $= "CRITICAL" ? "" : "%"));
	//Hunger
	switch(%hunger)
	{
		case 10: %hungercolor = "FF0000";
		case 20: %hungercolor = "FD2000";
		case 30: %hungercolor = "FF5900";
		case 40: %hungercolor = "FD7E00";
		case 50: %hungercolor = "FD7E00";
		case 60: %hungercolor = "F7FD00";
		case 70: %hungercolor = "C6FF00";
		case 80: %hungercolor = "7EFD00";
		case 90: %hungercolor = "73FF00";
		case 100: %hungercolor = "22FF00";
	}
	HungerText.setText(%textmlchars @ "Hunger" @ " <color:999999> - " SPC "<color:" @ %hungercolor @ ">" @ %hunger @ "%");
}

function CM_HUD::toggleInventory(%this, %step)
{
	%maxsteps = firstWord(InventoryBox.getExtent());
	%increment = 3;

	if(%step $= "")
	{
		InventoryBox.isOpening = true;
		%step = (InventoryBox.isOpen ? 0 : %maxsteps);
	}

	if(!InventoryBox.isOpen)
	{
		if(%step > 0)
		{
			InventoryBox.resize((getWord(InventoryBox.getPosition(), 0) - %increment), getWord(InventoryBox.getPosition(), 1), getWord(InventoryBox.getExtent(), 0), getWord(InventoryBox.getExtent(), 1));
			%this.schedule(10, "toggleInventory", %step -= %increment);
		}
		else
		{
			InventoryBox.isOpen = true;
			InventoryBox.isOpening = false;
		}
		InventoryBox.refreshAllValues();
	}
	else if(InventoryBox.isOpen)
	{
		if(%step < %maxsteps)
		{
			InventoryBox.resize((getWord(InventoryBox.getPosition(), 0) + %increment), getWord(InventoryBox.getPosition(), 1), getWord(InventoryBox.getExtent(), 0), getWord(InventoryBox.getExtent(), 1));
			%this.schedule(10, "toggleInventory", %step += %increment);
		}
		else
		{
			InventoryBox.isOpen = false;
			InventoryBox.isOpening = false;
		}
		InventoryBox.refreshAllValues();
	}
}

function CM_HUD::clickInventoryButton(%this, %slot)
{
	commandtoserver('CM_dropInventory', %slot);
}