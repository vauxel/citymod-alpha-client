// ============================================================
// Author				-		Ephialtes
// Description			-		RTB Notification Manager
// ============================================================

// ============================================================
//  Notification Manager
// ============================================================
function CM_createNotificationManager()
{
   if(isObject(CM_NotificationManager))
   {
      CM_NotificationManager.destroy();
      CM_NotificationManager.delete();
   }
   
   %manager = new ScriptGroup(CM_NotificationManager);
   //RTBGroup.add(%manager);
   
   return %manager;
}

function CM_NotificationManager::push(%this,%title,%message,%icon,%key,%holdTime)
{
   if(%key !$= "")
   {
      for(%i=0;%i<%this.getCount();%i++)
      {
         %notification = %this.getObject(%i);
         if(%notification.key $= %key)
         {
            %notification.icon = %icon;
            %notification.title = %title;
            %notification.message = %message;
            %notification.render();
            return;
         }
      }
   }
   
   if(%icon $= "")
      %icon = "information";
      
   if(%holdTime $= "")
      %holdTime = 5000;
   
   %notification = new ScriptObject()
   {
      class = "CM_Notification";
      
      key = %key;
      icon = %icon;
      title = "|CM|" SPC %title;
      message = %message;
      
      holdTime = %holdTime;
      
      state = "left";
   };
   %this.add(%notification);
   
   %notification.render();
}

function CM_NotificationManager::pop(%this,%key)
{
   for(%i=0;%i<%this.getCount();%i++)
   {
      %notification = %this.getObject(%i);
      if(%notification.key $= %key)
      {
         %notification.state = "right";
         %notification.step();
         break;
      }
   }
}

function CM_NotificationManager::refocus(%this)
{
   for(%i=0;%i<%this.getCount();%i++)
   {
      %notification = %this.getObject(%i);
      if(isObject(%notification.canvas) && %notification.canvas.script $= %notification)
         Canvas.getObject(Canvas.getCount()-1).add(%notification.canvas);
   }
}

function CM_NotificationManager::destroy(%this)
{
   for(%i=0;%i<%this.getCount();%i++)
   {
      %notification = %this.getObject(%i);
      if(isObject(%notification.canvas) && %notification.canvas.script $= %notification)
      {
         cancel(%notification.moveAnim);
         %notification.canvas.delete();
      }
   }
   %this.clear();
}

function CM_Notification::render(%this)
{
   %width = 209;
   %height = 50;
   
   if(isObject(%this.canvas) && %this.canvas.script $= %this)
   {
      %this.setIcon(%this.icon);
      %this.setTitle(%this.title);
      %this.setMessage(%this.message);
      
      cancel(%this.moveAnim);
      %this.state = "left";
      
      %this.step();
   }
   else
   {
      %xPosition = getWord(getRes(),0) - %width;
      %yPosition = getWord(getRes(),1) - %height;
      
      %manager = %this.getGroup();
      for(%i=0;%i<%manager.getCount();%i++)
      {
         %notification = %manager.getObject(%i);
         if(isObject(%notification.canvas) && %notification.canvas.script $= %notification)
         {
            if(getWord(%notification.canvas.position,1) <= %yPosition)
               %yPosition = getWord(%notification.canvas.position,1)-%height;
         }
      }
      
      if(%yPosition < 0)
         return;
      
      %canvas = new GuiSwatchCtrl()
      {
         position = %xPosition SPC %yPosition;
         extent = %width SPC %height;
         color = "0 0 0 0";
         
         script = %this;
      };
      Canvas.getObject(canvas.getCount()-1).add(%canvas);
      
      %this.canvas = %canvas;
      
      if(canvas.getContent().getName() $= "PlayGui")
         %this.drawPlay();
      else
         %this.drawMenu();
         
      %this.step();
   }
}

function CM_Notification::step(%this)
{
   if(%this.state $= "left")
   {
      if(getWord(%this.window.position,0) <= 0)
      {
         if(%this.holdTime < 0)
         {
            %this.window.position = "0 0";
            %this.state = "done";
            return;
         }
         %this.window.position = "0 0";
         %this.state = "wait";
         %this.moveAnim = %this.schedule(%this.holdTime,"step");
         return;
      }
      %this.window.position = vectorSub(%this.window.position,"10 0");
      %this.moveAnim = %this.schedule(10,"step");
   }
   else if(%this.state $= "wait")
   {
      %this.state = "right";
      %this.step();
   }
   else if(%this.state $= "right")
   {
      if(getWord(%this.window.position,0) >= getWord(%this.canvas.extent,0))
      {
         %this.window.position = getWord(%this.canvas.extent,0) SPC "0";
         %this.state = "done";
         %this.step();
         return;
      }
      %this.window.position = vectorAdd(%this.window.position,"10 0");
      %this.moveAnim = %this.schedule(10,"step");
   }
   else if(%this.state $= "done")
   {
      %y = getWord(%this.canvas.position,1);
      %this.canvas.delete();

      for(%i=0;%i<CM_NotificationManager.getCount();%i++)
      {
         %notification = CM_NotificationManager.getObject(%i);
         if(!isObject(%notification.canvas))
            continue;
         if(getWord(%notification.canvas.position,1) < %y)
            %notification.canvas.shift(0,50);
      }
      %this.delete();
   }
}

function CM_Notification::drawPlay(%this)
{
   %draw = new GuiBitmapCtrl() {
      position = getWord(%this.canvas.extent,0) SPC "0";
      extent = "204 44";
      bitmap = "Add-Ons/Client_CityMod/Assets/UI/notificationPlay";
      
      new GuiBitmapCtrl() {
         position = "13 12";
         extent = "16 16";
         bitmap = "Add-Ons/Client_CityMod/Assets/Icons/" @ %this.icon;
      };
      new GuiMLTextCtrl() {
         position = "41 5";
         extent = "154 14"; 
         text = "<shadow:2:2><shadowcolor:00000066><color:EEEEEE><font:Verdana Bold:15>"@%this.title;
         selectable = false;
      };
      new GuiMLTextCtrl() {
         position = "41 21";
         extent = "165 12";
         text = "<shadow:2:2><shadowcolor:00000066><color:DDDDDD><font:Verdana:12>"@%this.message;
         selectable = false;
      };
      new GuiBitmapButtonCtrl() {
         position = "0 0";
         extent = "204 44";
         text = " ";
         command = "";
      };
   };
   %this.canvas.add(%draw);

   %this.window = %draw;
}

function CM_Notification::drawMenu(%this)
{
   %draw = new GuiBitmapCtrl() {
      position = getWord(%this.canvas.extent,0) SPC "0";
      extent = "204 44";
      bitmap = "Add-Ons/Client_CityMod/Assets/UI/notificationPlay";
      
      new GuiBitmapCtrl() {
         position = "13 12";
         extent = "16 16";
         bitmap = "Add-Ons/Client_CityMod/Assets/Icons/" @ %this.icon;
      };
      new GuiMLTextCtrl() {
         position = "41 5";
         extent = "154 14"; 
         text = "<shadow:2:2><shadowcolor:00000066><color:EEEEEE><font:Verdana Bold:15>"@%this.title;
         selectable = false;
      };
      new GuiMLTextCtrl() {
         position = "41 21";
         extent = "165 12";
         text = "<shadow:2:2><shadowcolor:00000066><color:DDDDDD><font:Verdana:12>"@%this.message;
         selectable = false;
      };
      new GuiBitmapButtonCtrl() {
         position = "0 0";
         extent = "204 44";
         text = " ";
         command = "";
      };
   };
   %this.canvas.add(%draw);

   %this.window = %draw;
}

function CM_Notification::setIcon(%this,%icon)
{
   %this.window.getObject(0).setBitmap("Add-Ons/Client_CityMod/Assets/Icons/"@%icon);
}

function CM_Notification::setTitle(%this,%title)
{
   %this.window.getObject(1).setText("<shadow:2:2><shadowcolor:00000066><color:EEEEEE><font:Verdana Bold:15>"@%title);
} 

function CM_Notification::setMessage(%this,%message)
{
   %this.window.getObject(2).setText("<shadow:2:2><shadowcolor:00000066><color:DDDDDD><font:Verdana:12>"@%message);
}

CM_createNotificationManager();