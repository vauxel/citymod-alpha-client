// ============================================================
// Author				-		Vaux
// Description			-		Code for the Bank GUI
// ============================================================
exec("Add-Ons/Client_CityMod/guis/CM_Police.gui");

function clientcmdCM_clearCasesList()
{
	CasesList.deleteAll();
	CasesList.resize(1, 1, 222, 98);
}

function clientcmdCM_setCriminalsList(%crim, %clear)
{
	if(%crim $= "")
		return;

	if(%clear)
		CriminalAPBText.setText(CriminalAPBText.text);

	if(stripMLControlChars(CriminalAPBText.text) $= "Criminals APB: NONE")
		CriminalAPBText.setText("<just:center>Criminals APB:" SPC stripMLControlChars(%crim));
	else
		CriminalAPBText.setText(CriminalAPBText.text @ "," SPC stripMLControlChars(%crim));
}

function clientcmdCM_addCrimesList(%crime, %id, %type, %cluesCount, %investigator, %iname)
{
	if(%investigator $= "" || %iname $= "")
		%iname = "NOBODY";

	$CityModClient::Temp::CrimeData[%id] = new scriptObject()
	{
		Class = "CMCrimesData";
		
		CrimeID = %crime;
		CrimeNumber = %id;
		Type = %type;
		Clues = %cluesCount;
		Investigator = %investigator;
		InvestigatorName = %iname;
	};

	%scroll = "CasesList";

	if(%scroll.getCount() <= 0)
		%pos = "1 0";
	else
		%pos = 1 SPC getWord(%scroll.getObject(%scroll.getCount() - 1).position, 1) + getWord(%scroll.getObject(%scroll.getCount() - 1).extent, 1) + 2;

	%skillbox = new GuiBitmapBorderCtrl()
	{
		profile = "RTB_ContentBorderProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = %pos;
		extent = "220 31";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
	};

	%bg = new GuiSwatchCtrl()
	{
		profile = "GuiDefaultProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "1 2";
		extent = "218 27";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		color = "255 255 255 255";
	};

	%casetext = new GuiMLTextCtrl()
	{
		profile = "CMScrollProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "3 5";
		extent = "82 13";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		lineSpacing = "2";
		allowColorChars = "0";
		maxChars = "-1";
		text = "Case #" @ %id;
		maxBitmapHeight = "-1";
		selectable = "1";
		autoResize = "1";
	};

	%cluestext = new GuiMLTextCtrl()
	{
		profile = "CMTextEditProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "74 0";
		extent = "141 12";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		lineSpacing = "2";
		allowColorChars = "0";
		maxChars = "-1";
		text = "<just:right>Clues:" SPC %cluesCount;
		maxBitmapHeight = "-1";
		selectable = "1";
		autoResize = "1";
	};

	%investigatortext = new GuiMLTextCtrl()
	{
		profile = "CMTextEditProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "74 13";
		extent = "141 12";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		lineSpacing = "2";
		allowColorChars = "0";
		maxChars = "-1";
		text = "<just:right>Investigator:" SPC %iname;
		maxBitmapHeight = "-1";
		selectable = "1";
		autoResize = "1";
	};

	%button = new GuiBitmapButtonCtrl()
	{
		profile = "CMBitmapButtonProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "0 0";
		extent = "220 31";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		command = "$CityModClient::Temp::SelectedCase = " @ %id @ "; echo(" @ %id @ ");";
		text = "`";
		groupNum = "-1";
		buttonType = "PushButton";
		bitmap = "base/data/shapes/blank.png";
		lockAspectRatio = "0";
		alignLeft = "0";
		alignTop = "0";
		overflowImage = "0";
		mKeepCached = "0";
		mColor = "255 255 255 255";
	};

	//Add the necessary elements
	%bg.add(%casetext);
	%bg.add(%cluestext);
	%bg.add(%investigatortext);
	%skillbox.add(%bg);
	%skillbox.add(%button);
	%skillbox.setVisible(1);

	if(%scroll.getCount() >= 3)
	{
		//Re-Adjust the Swatch
		%guiExtY = getWord(%scroll.extent, 1);
		%swatchExtY = getWord(%skillbox.extent, 1);
		%swatchPosY = getWord(%skillbox.position, 1);

		%extX = getWord(%scroll.extent, 0);
		%extY = %guiExtY + %swatchExtY + 1;
		%scroll.extent = %extX SPC %extY;
	}

	//Refresh the Swatch
	%scroll.add(%skillbox);
	%scroll.setVisible(1);
}

function CM_Police::onWake(%this)
{
	clientcmdCM_clearCasesList();
	$CityModClient::Temp::SelectedCase = "-1";

	commandtoserver('CM_requestCases');
	commandtoserver('CM_requestCriminals');
}

function CM_Police::reportEvidence(%this)
{
	%id = $CityModClient::Temp::SelectedCase;
	%data = $CityModClient::Temp::CrimeData[%id];
	
	if($CityModClient::Temp::SelectedCase $= "-1")
		return clientcmdCM_pushNotifyBox("Police Station", "You didn't select a Case to report evidence for!");
	
	if(!isObject(%data))
		return clientcmdCM_pushNotifyBox("Police Station", "Somehow you screwed up, you didn't select a real case.");
	
	commandtoserver('CM_reportEvidence', %data.CrimeID, %data.CrimeNumber);
}

function CM_Police::claimCase(%this)
{
	%id = $CityModClient::Temp::SelectedCase;
	%data = $CityModClient::Temp::CrimeData[%id];
	
	if($CityModClient::Temp::SelectedCase $= "-1")
		return clientcmdCM_pushNotifyBox("Police Station", "You didn't select a Case to claim!");

	if(!isObject(%data))
		return clientcmdCM_pushNotifyBox("Police Station", "Somehow you screwed up, you didn't select a real case.");

	if(%data.Investigator !$= "")
		return clientcmdCM_pushNotifyBox("Police Station", "This case has already been claimed!");
	
	commandtoserver('CM_claimCase', %data.CrimeID, %data.CrimeNumber);
}

function CM_Police::solveCase(%this)
{
	%id = $CityModClient::Temp::SelectedCase;
	%data = $CityModClient::Temp::CrimeData[%id];
	
	if($CityModClient::Temp::SelectedCase $= "-1")
		return clientcmdCM_pushNotifyBox("Police Station", "You didn't select a Case to solve!");

	if(!isObject(%data))
		return clientcmdCM_pushNotifyBox("Police Station", "Somehow you screwed up, you didn't select a real case.");
	
	commandtoserver('CM_solveCase', %data.CrimeID, %data.CrimeNumber);

	$CityModClient::Temp::SelectedCase = "-1";
}