// ============================================================
// Author				-		Vaux
// Description			-		Code for the Machinery GUI
// ============================================================

exec("Add-Ons/Client_CityMod/guis/CM_Checkout.gui");

function clientcmdCM_setRecipeConcocting(%recipe1, %recipe2, %recipe3, %recipe4)
{
	$CityModClient::Temp::RecipeData[%owner] = new scriptObject()
	{
		Class = "CMRecipeData";
		
		Recipe[1] = %recipe1;
		Recipe[2] = %recipe2;
		Recipe[3] = %recipe3;
		Recipe[4] = %recipe4;
	};

	if(%recipe1 !$= "Empty")
		Recipe1Button.setText("45% Quality | 3 Food");
	if(%recipe2 !$= "Empty")
		Recipe2Button.setText("45% Quality | 3 Food");
	if(%recipe3 !$= "Empty")
		Recipe3Button.setText("45% Quality | 3 Food");
	if(%recipe4 !$= "Empty")
		Recipe4Button.setText("45% Quality | 3 Food");
}

function CM_Recipe::saveRecipe()
{
	if(RecipeProgressBar.isInUse)
		return;

	if(RecipeIngredient1Bar.getValue() == 0 && RecipeIngredient2Bar.getValue() == 0 && RecipeIngredient3Bar.getValue() == 0 && RecipeIngredient4Bar.getValue() == 0)
		return clientcmdCM_pushNotifyBox("Recipe Concocting", "You can't make a blank Recipe!\nModify the Quality Bars below!");

	CM_Recipe.makeRecipeLoop(10);
}

function CM_Recipe::makeRecipeLoop(%count)
{
	if(%count $= "")
		return;

	RecipeProgressBar.isInUse = true;
	if(%count == 0)
	{
		RecipeProgressBar.isInUse = false;
		commandtoserver('CM_saveRecipe');
	}
	else
	{
		RecipeProgressBar.setValue(RecipeProgressBar.getValue() + 0.1);
		CM_Recipe.schedule(500, "makeRecipeLoop", %count--);
	}
}

function CM_Recipe::modifyBar(%num, %mode)
{
	switch(%num)
	{
		case 1:
			if(%mode $= "add")
			{
				if(RecipeIngredient1Bar.getValue() != 1)
					RecipeIngredient1Bar.setValue(RecipeIngredient1Bar.getValue() + 0.1);
			}
			else if(%mode $= "remove")
			{
				if(RecipeIngredient1Bar.getValue() != 0)
					RecipeIngredient1Bar.setValue(RecipeIngredient1Bar.getValue() - 0.1);
			}
		case 2:
			if(%mode $= "add")
			{
				if(RecipeIngredient2Bar.getValue() != 1)
					RecipeIngredient2Bar.setValue(RecipeIngredient2Bar.getValue() + 0.1);
			}
			else if(%mode $= "remove")
			{
				if(RecipeIngredient2Bar.getValue() != 0)
					RecipeIngredient2Bar.setValue(RecipeIngredient2Bar.getValue() - 0.1);
			}
		case 3:
			if(%mode $= "add")
			{
				if(RecipeIngredient3Bar.getValue() != 1)
					RecipeIngredient3Bar.setValue(RecipeIngredient3Bar.getValue() + 0.1);
			}
			else if(%mode $= "remove")
			{
				if(RecipeIngredient3Bar.getValue() != 0)
					RecipeIngredient3Bar.setValue(RecipeIngredient3Bar.getValue() - 0.1);
			}
		case 4:
			if(%mode $= "add")
			{
				if(RecipeIngredient4Bar.getValue() != 1)
					RecipeIngredient4Bar.setValue(RecipeIngredient4Bar.getValue() + 0.1);
			}
			else if(%mode $= "remove")
			{
				if(RecipeIngredient4Bar.getValue() != 0)
					RecipeIngredient4Bar.setValue(RecipeIngredient4Bar.getValue() - 0.1);
			}
	}
}

function CM_Checkout::onWake(%this)
{
	commandtoserver('CM_requestRecipes');
}