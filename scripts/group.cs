// ============================================================
// Author				-		Vaux
// Description			-		Code for the Group GUI
// ============================================================

exec("Add-Ons/Client_CityMod/guis/CM_Group.gui");

function clientcmdCM_setGroupList(%name, %membercount)
{
	$CityModClient::Temp::GroupData[%name] = new scriptObject()
	{
		Class = "CMGroupData";
		
		Members = %membercount;
		MemberList = "";
	};

	GroupsList.addRow(1, %name @ "                                       " @ %membercount);
}

function clientcmdCM_setGroupMember(%group, %name, %rank)
{
	if(isObject($CityModClient::Temp::GroupData[%group]))
		$CityModClient::Temp::GroupData[%group].MemberList = ltrim($CityModClient::Temp::GroupData[%group].MemberList SPC %name);

	GroupMemberList.addRow(1, %name @ "        " @ %rank);
}

function clientcmdCM_setGroupManagement()
{
	if(isObject($CityModClient::Temp::GroupData[%group]))
		$CityModClient::Temp::GroupData[%group].MemberList = ltrim($CityModClient::Temp::GroupData[%group].MemberList SPC %name);

	GroupMemberList.addRow(1, %name @ "        " @ %rank);
}

function clientcmdCM_setGroupPlayerInfo(%ranklevel, %rankname, %salary)
{
	if(%ranklevel $= "-1")
	{
		GroupRankText.setText("<just:center>You are not in");
		GroupSalaryText.setText("<just:center>this Organization");
	}
	else if(%ranklevel $= "-2")
	{
		GroupRankText.setText("<just:center>You are this group's");
		GroupSalaryText.setText("<just:center>Owner");
	}
	else
	{
		GroupRankText.setText("<just:center>Rank:" SPC %ranklevel SPC "(" @ %rankname @ ")");
		GroupSalaryText.setText("<just:center>Salary:" SPC "$" @ %salary);
	}
}

function clientcmdCM_setGroupStockInfo(%stocksleft, %stockholderamt, %stockprice)
{
	GroupStockRemaining.setText("Stocks Remaining:" SPC %stocksleft);
	GroupStockHolders.setText("Stock Holders:" SPC %stockholderamt);
	GroupStockPrice.setText("Stock Price:" SPC %stockprice);
}

function clientcmdCM_setGroupManagementInfo(%name, %owner, %funds)
{
	GroupManagementWindow.setVisible(1);
	GroupManagementRankList.clear();

	ManageGroupName.setText(%name);
	ManageGroupOwner.setText(%owner);
	GroupFundsText.setText("<just:center>Total Funds:" SPC "$" @ condenseValue(%funds));
}

function clientcmdCM_closeGroupManagement()
{
	GroupManagementRankList.clear();
	GroupManagementWindow.setVisible(0);
}

function clientcmdCM_clearGroupManagementRanks()
{
	GroupManagementRankList.clear();
}

function clientcmdCM_refreshGroupsList()
{
	GroupsList.clear();
	GroupMemberList.clear();

	commandtoserver('CM_requestGroups');
}

function clientcmdCM_setGroupManagementRank(%level, %name, %salary, %clear)
{
	$CityModClient::Temp::GroupRankData[%level] = new scriptObject()
	{
		Class = "CMGroupRankData";
		
		Name = %name;
		Salary = %salary;
	};

	GroupManagementRankList.addRow(1, "(" @ %level @ ")" SPC %name SPC "-" SPC "$" @ condenseValue(%salary));
}

function clientcmdCM_setInvestmentInfo(%name, %ownedamt, %price)
{
	GroupInvestName.setText("<just:center><font:Berlin Sans FB Demi:18><color:3C3C3C>" @ %name);
	GroupStocksAmount.setText("<just:center>Stocks Owned:" SPC %ownedamt);
	GroupStocksPrice.setText("<just:center>Stock Price:" SPC %price);

	GroupInvestmentWindow.setVisible(1);
}

function GroupManagementRankList::onSelect(%this)
{
	%level = getWord(GroupManagementRankList.getValue(), 0);
	%level = strReplace(%level, "(", "");
	%level = strReplace(%level, ")", "");

	ManageGroupRankName.setText("Name:" SPC $CityModClient::Temp::GroupRankData[%level].Name);
	ManageGroupRankSalary.setText("Salary:" SPC "$" @ $CityModClient::Temp::GroupRankData[%level].Salary);
}

function GroupsList::onSelect(%this)
{
	GroupMemberList.clear();
	commandtoserver('CM_requestGroupMembers', getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1)));
}

function CM_Group::createGroup()
{
	%name = GroupCreationName.getValue();
	%funds = GroupStarterFunds.getValue();
	%funds = strReplace(%funds, "$", "");

	%starterrank_name = GroupStarterRankName.getValue();
	%starterrank_salary = GroupStarterRankSalary.getValue();
	%starterrank_salary = strReplace(%starterrank_salary, "$", "");

	commandtoserver('CM_createGroup', %name, %funds, %starterrank_name, %starterrank_salary);
}

function CM_Group::manageGroup()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));

	if(%group $= "")
		return clientcmdCM_pushNotifyBox("Organizations", "You didn't select an Organization to manage!");

	commandtoserver('CM_manageGroup', %group);
}

function CM_Group::investInGroup()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));

	if(%group $= "")
		return clientcmdCM_pushNotifyBox("Organizations", "You didn't select an Organization to invest in!");

	commandtoserver('CM_investInGroup', %group);
}

function CM_Group::joinGroup()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));

	if(%group $= "")
		return clientcmdCM_pushNotifyBox("Organizations", "You didn't select an Organization to join!");

	commandtoserver('CM_joinGroup', %group);
}

function CM_Group::leaveGroup()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));

	if(%group $= "")
		return clientcmdCM_pushNotifyBox("Organizations", "You didn't select an Organization to leave!");

	commandtoserver('CM_leaveGroup', %group);
}

function CM_Group::kickPlayer()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));
	%victim = getWord(GroupMemberList.getValue(), 0);

	if(%victim $= "")
		return clientcmdCM_pushNotifyBox("Organizations", "You didn't select a Player to kick!");

	commandtoserver('CM_kickFromGroup', %group, %victim);
}

function CM_Group::submitMainChanges()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));
	%title = ManageGroupName.getValue();
	%owner = ManageGroupOwner.getValue();

	if(%title $= "ERROR" || %owner $= "ERROR")
		return;

	commandtoserver('CM_changeGroupMain', %group, %title, %owner);
}

function CM_Group::saveRank()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));
	%rank = getWord(GroupManagementRankList.getValue(), 0);
	%rank = strReplace(%rank, "(", "");
	%rank = strReplace(%rank, ")", "");

	%name = ManageGroupRankName.getValue();
	%salary = ManageGroupRankSalary.getValue();
	%salary = strReplace(%salary, "$", "");

	if(%name $= ManageGroupRankName.text || %salary $= ManageGroupRankSalary.text)
		return;

	if(%rank $= "")
		return clientcmdCM_pushNotifyBox("Organizations", "You didn't select a Rank to edit!");

	if(!isInteger(%salary))
		return clientcmdCM_pushNotifyBox("Organizations", %salary SPC "is not a valid number for a Rank's Salary");

	commandtoserver('CM_editGroupRank', %group, %rank, %name, %salary);
}

function CM_Group::createRank()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));

	%name = ManageGroupRankName.getValue();
	%salary = ManageGroupRankSalary.getValue();
	%salary = strReplace(%salary, "$", "");

	if(%name $= ManageGroupRankName.text || %salary $= ManageGroupRankSalary.text)
		return;

	if(!isInteger(%salary))
		return clientcmdCM_pushNotifyBox("Organizations", %salary SPC "is not a valid number for a Rank's Salary");

	commandtoserver('CM_createGroupRank', %group, %name, %salary);
}

function CM_Group::changeRank()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));
	%player = ChangePlayerRank.getValue();
	%rank = getWord(GroupManagementRankList.getValue(), 0);
	%rank = strReplace(%rank, "(", "");
	%rank = strReplace(%rank, ")", "");

	if(!isInteger(%player))
		return clientcmdCM_pushNotifyBox("Organizations", "You didn't enter a valid BL_ID!");

	if(%rank $= "")
		return clientcmdCM_pushNotifyBox("Organizations", "You didn't select a Rank to move the player to!");

	commandtoserver('CM_changeGroupRank', %group, %player, %rank);
}

function CM_Group::deleteRank()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));
	%rank = getWord(GroupManagementRankList.getValue(), 0);
	%rank = strReplace(%rank, "(", "");
	%rank = strReplace(%rank, ")", "");

	if(%rank $= "")
		return clientcmdCM_pushNotifyBox("Organizations", "You didn't select a Rank to delete!");

	commandtoserver('CM_deleteGroupRank', %group, %rank);
}

function CM_Group::depositFunds()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));

	%amount = ManageGroupFunds.getValue();
	%amount = strReplace(%amount, "$", "");

	if(%amount $= "$0")
		return;

	if(!isInteger(%amount))
		return clientcmdCM_pushNotifyBox("Organizations", %amount SPC "is not a valid number");

	commandtoserver('CM_depositGroupFunds', %group, %amount);
}

function CM_Group::withdrawFunds()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));

	%amount = ManageGroupFunds.getValue();
	%amount = strReplace(%amount, "$", "");

	if(%amount $= "$0")
		return;

	if(!isInteger(%amount))
		return clientcmdCM_pushNotifyBox("Organizations", %amount SPC "is not a valid number");

	commandtoserver('CM_withdrawGroupFunds', %group, %amount);
}

function CM_Group::enterStockMarket()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));
	%stockamount = GroupStockStartingAmount.getValue();

	if(%stockamount $= "0")
		return;

	if(!isInteger(%stockamount))
		return clientcmdCM_pushNotifyBox("Organizations", %stockamount SPC "is not a valid number");

	commandtoserver('CM_enterStockMarket', %group, %stockamount);
}

function CM_Group::leaveStockMarket()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));
	commandtoserver('CM_leaveStockMarket', %group);
}

function CM_Group::buyStocks()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));
	%amount = GroupInvestAmount.getValue();

	if(%amount $= "0")
		return;

	if(!isInteger(%amount))
		return clientcmdCM_pushNotifyBox("Organizations", %amount SPC "is not a valid number");

	commandtoserver('CM_buyGroupStock', %group, %amount);
}

function CM_Group::sellStocks()
{
	%group = getWords(GroupsList.getValue(), 0, getWordCount(GroupsList.getValue() - 1));
	%amount = GroupInvestAmount.getValue();

	if(%amount $= "0")
		return;

	if(!isInteger(%amount))
		return clientcmdCM_pushNotifyBox("Organizations", %amount SPC "is not a valid number");

	commandtoserver('CM_sellGroupStock', %group, %amount);
}

function CM_Group::showStartingStockInfo()
{
	clientcmdCM_pushNotifyBox("Organizations", 
		"This number represents the amount of Stocks you want your company to start out with upon entering the Stock Market." NL
		"The higher the number, the bigger the supply of Stocks, meaning that more people will be able to buy into your company." NL
		"But this also means that the price /per stock will be more thinly spread."
	);
}

function CM_Group::onWake(%this)
{
	GroupsList.clear();
	GroupMemberList.clear();

	GroupManagementWindow.setVisible(0);
	GroupCreationWindow.setVisible(0);

	commandtoserver('CM_requestGroups');
}