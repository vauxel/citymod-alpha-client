// ============================================================
// Author				-		Vaux
// Description			-		Code for the Bank GUI
// ============================================================

exec("Add-Ons/Client_CityMod/guis/CM_Bank.gui");

function clientcmdCM_setBank(%bank, %wallet, %debt)
{
	BankWalletValue.setText("Wallet:" SPC condenseValue(%wallet));
	BankValue.setText("Bank:" SPC condenseValue(%bank));
	//BankDebtValue.setText("Debt:" SPC condenseValue(%debt));
}

function CM_Bank::onWake(%this)
{
	commandtoserver('CM_getBankStats');
}

function CM_Bank::deposit(%this, %all)
{
	%value = BankMoneyInput.getValue();
	%value = strReplace(%value, "$", "");

	if(!isInteger(%value))
		return clientcmdCM_pushNotifyBox("Bank", "You didn't enter a valid amount!");

	if(%all)
		commandtoserver('CM_depositBank', "-1");
	else
		commandtoserver('CM_depositBank', %value);
}

function CM_Bank::withdraw(%this, %all)
{
	%value = BankMoneyInput.getValue();
	%value = strReplace(%value, "$", "");

	if(!isInteger(%value))
		return clientcmdCM_pushNotifyBox("Bank", "You didn't enter a valid amount!");
	
	if(%all)
		commandtoserver('CM_withdrawBank', "-1");
	else
		commandtoserver('CM_withdrawBank', %value);
}