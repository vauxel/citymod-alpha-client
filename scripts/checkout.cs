// ============================================================
// Author				-		Vaux
// Description			-		Code for the Machinery GUI
// ============================================================

exec("Add-Ons/Client_CityMod/guis/CM_Checkout.gui");

function clientcmdCM_setCashRegister(%owner, %foods, %guns, %pricing, %tax)
{
	$CityModClient::Temp::CheckoutData[%owner] = new scriptObject()
	{
		Class = "CMCheckoutData";
		
		Owner = %owner;
		Tax = %tax;

		Prices["Tiny"] = getWord(%pricing, 0);
		Prices["Small"] = getWord(%pricing, 1);
		Prices["Medium"] = getWord(%pricing, 2);
		Prices["Large"] = getWord(%pricing, 3);
		Prices["Oversized"] = getWord(%pricing, 4);
		Prices["XXL"] = getWord(%pricing, 5);
	};

	CheckoutItemsList.clear();
	CartItemsText.setText("<just:center><font:Impact:16><color:3E3E3E>You currently have" SPC (getWordCount(%foods) + getWordCount(%guns)) SPC "Items in your Cart");
	CheckoutWindow.setText(%owner @ "'s Store");

	%foodquantities["Tiny"] = 0;
	%foodquantities["Small"] = 0;
	%foodquantities["Medium"] = 0;
	%foodquantities["Large"] = 0;
	%foodquantities["Oversized"] = 0;
	%foodquantities["XXL"] = 0;

	for(%i = 0; %i < getWordCount(%foods); %i++)
	{
		%foodquantities[getWord(%foods, %i)]++;
	}

	if(%foodquantities["Tiny"] != 0)
	{
		CheckoutItemsList.addRow(1, %foodquantities["Tiny"] @ ") TINY Serving of Food");
		%cost += $CityModClient::Temp::CheckoutData[%owner].Prices["Tiny"];
	}

	if(%foodquantities["Small"] != 0)
	{
		CheckoutItemsList.addRow(1, %foodquantities["Small"] @ ") SMALL Serving of Food");
		%cost += $CityModClient::Temp::CheckoutData[%owner].Prices["Small"];
	}

	if(%foodquantities["Medium"] != 0)
	{
		CheckoutItemsList.addRow(1, %foodquantities["Medium"] @ ") MEDIUM Serving of Food");
		%cost += $CityModClient::Temp::CheckoutData[%owner].Prices["Medium"];
	}

	if(%foodquantities["Large"] != 0)
	{
		CheckoutItemsList.addRow(1, %foodquantities["Large"] @ ") LARGE Serving of Food");
		%cost += $CityModClient::Temp::CheckoutData[%owner].Prices["Large"];
	}

	if(%foodquantities["Oversized"] != 0)
	{
		CheckoutItemsList.addRow(1, %foodquantities["Oversized"] @ ") OVERSIZED Serving of Food");
		%cost += $CityModClient::Temp::CheckoutData[%owner].Prices["Oversized"];
	}

	if(%foodquantities["XXL"] != 0)
	{
		CheckoutItemsList.addRow(1, %foodquantities["XXL"] @ ") XXL Serving of Food");
		%cost += $CityModClient::Temp::CheckoutData[%owner].Prices["XXL"];
	}

	CheckoutItemsList.addRow(1, "   =" SPC "$" @ %cost);
	CheckoutItemsList.addRow(1, "----------------------------------------------");
	CheckoutItemsList.addRow(1, %tax @ "% Tax of" SPC %cost SPC "($" @ mFloor(%cost * (%tax / 100)) @ ")");
	%cost += mFloor(%cost * (%tax / 100));
	CheckoutItemsList.addRow(1, "   =" SPC "$" @ %cost);
	CheckoutItemsList.addRow(1, "----------------------------------------------");

	CartCostText.setText("<just:center><font:Impact:26><color:3E3E3E>Your Total: $" @ %cost);
}

function CM_Checkout::onSleep(%this)
{
	commandtoserver('CM_removeCashRegister');
}

function CM_Checkout::onWake(%this)
{
	commandtoserver('CM_requestCashRegister');
}